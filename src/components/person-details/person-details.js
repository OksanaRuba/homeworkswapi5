import React, { useContext, useState, useEffect } from 'react';
import {NameContext} from '../app/app';

import './person-details.css';

const PersonDetails = () => {
const[information, setInformation] = useState([])

console.warn('information', information);

const {profileInformation} = useContext(NameContext);
console.warn('profileInformation', profileInformation)

useEffect(() => {
  setInformation(profileInformation)
},[profileInformation]) 

    return (
      <div className="person-details card">
        <img className="person-image"
          src="https://starwars-visualguide.com/assets/img/characters/3.jpg" />

        <div className="card-body">
          <h4>R2-D2</h4>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              <span className="term">Gender</span>
              <span>{information.gender}</span>
            </li>
            <li className="list-group-item">
              <span className="term">Birth Year </span>
              <span>{information.birth_year}</span>
            </li>
            <li className="list-group-item">
              <span className="term">Eye Color</span>
              <span>{information.eye_color}</span>
            </li>
          </ul>
        </div>
      </div>
    )
  }

export default PersonDetails;